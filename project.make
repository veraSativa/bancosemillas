core = 7.x
api = 2

; Drupal Core
projects[drupal] = "7.50"


; =====================================
; Contrib Modules
; =====================================

; By default, store all contrib modules in the "contrib" subdirectory of the
; modules directory.

defaults[projects][subdir] = "contrib"

; ===================
; === Base Drupal ===
; ===================

projects[libraries][version] = "2.3"
projects[token][version] = "1.6"
projects[entity][version] = "1.7"
projects[rules][version] = "2.9"
projects[ctools][version] = "1.10"
projects[session_api][version] = "1.0-rc1"


projects[views][version] = "3.14"
; Para que se vean las putas imagenes
projects[views][patch][] = patches/views-add-field-post-processing-when-grouping-20151231.patch

; === Fields ===
; ==============

projects[addressfield][version] = "1.2"
projects[entityreference][version] = "1.1"
projects[date][version] = "2.9"
projects[link][version] = "1.4"
projects[range][version] = "1.6"
projects[field_group][version] = "1.5"
projects[multistep][version] = "1.x-dev"
projects[auto_entitylabel][version] = "1.3"
projects[markup][version] = "1.2"
projects[empty_fields][version] = "2.0"
projects[field_formatter_settings][version] = "1.1"

; === Features ===
; ================
; I need a commit from 12 nov 2015: https://www.drupal.org/node/1973098
projects[commerce_features][version] = "1.2"

;  Para configurar blocks trough features
projects[features_extra][version] = "1.0"
projects[features_site_frontpage][version] = "1.0"
projects[features_override][version] = "2.0-rc3"
projects[features][version] = "2.10"


; ==============
; Base store
; ==============


;projects[default_image_ft][version] = "1.4"

projects[commerce][version] = "1.13"
projects[commerce_message][version] = "1.0-rc4"
projects[commerce_shipping][version] = "2.2"
projects[commerce_costs_profits][version] = "1.0-rc2"
projects[inline_entity_form][version] = "1.8"

;  Variable (required by Costs and profits)
projects[variable][version] = "2.5"


; ==================
; ===== Search =====
; ==================
projects[commerce_search_api][version] = "1.4"
projects[search_api][version] = "1.16"
projects[search_api_solr][version] = "1.8"
projects[facetapi][version] = "1.5"
projects[facetapi_pretty_paths][version] = "1.4"
projects[search_api_ranges][version] = "1.5"
;projects[facetapi_ranges][version] = "1.x-dev"
projects[search_api_grouping][version] = "2.x-dev"
projects[search_api_autocomplete][version] = "1.3"

; Search API DB (only for dev)
projects[search_api_db][version] = "1.5"

; Search API spell check
projects[search_api_spellcheck][version] = "1.0"

; ===========
; === SEO ===
; ===========

projects[pathauto][version] = "1.3"
projects[transliteration][version] = "3.2"

; This 2 are supposed to be merge here:
; https://www.drupal.org/node/905914
projects[redirect][version] = "1.0-rc3"
projects[globalredirect][version] = "1.5"
projects[seo_checklist][version] = "4.x-dev"
projects[checklistapi][version] = "1.2"



; =============
; === UX/UI ===
; =============

; === Pendientes ===
; ==================
; Para imagenes responsivas https://www.drupal.org/project/picture
; responsive menu: https://drupalize.me/videos/configuring-responsive-menus-module
; para correo transaccional: https://www.drupal.org/project/campaignmonitor
; para nuevo "home temporal" https://www.drupal.org/project/splashify
;

; === Admin ===
; =============
projects[adminimal_theme][version] = "1.24"
projects[adminimal_admin_menu][version] = "1.7"
projects[coffee][version] = "2.2"

projects[admin_menu][version] = "3.0-rc5"
projects[module_filter][version] = "2.0"
projects[commerce_backoffice][version] = "1.5"
projects[r4032login][version] = "1.8"

; For mkt_tags
projects[entityconnect][version] = "1.0-rc5"



; === WYSIWYG ===
; ===============

projects[ckeditor][version] = "1.17"
projects[linkit][version] = "3.5"

projects[bueditor][version] = "1.8"
projects[bueditor_plus][version] = "1.4"
projects[markdown][version] = "1.4"
projects[markdowneditor][version] = "1.4"
projects[linkit_markdown][version] = "1.0"
projects[ajax_markup][version] = "1.1"



; === User understanding ===
; ==========================

projects[piwik][version] = "2.9"
projects[piwik_reports][version] = "3.0-rc1"


; === Spam control ===
; ====================

projects[mollom][version] = "2.15"

; === Front End ===
; =================


; need this: https://www.drupal.org/node/2149561
; projects[bootstrap][version] = "3.x-dev"
projects[bootstrap][version] = "3.6"
projects[views_bootstrap][version] = "3.1"

; so you can define the number of items per row for each breakpoints:
projects[views_bootstrap_responsive_grid][type] = module
projects[views_bootstrap_responsive_grid][download][type] = git
projects[views_bootstrap_responsive_grid][download][branch] = "7.x-1.x"
projects[views_bootstrap_responsive_grid][download][url] = https://git.drupal.org/sandbox/gido/2456873.git

; solved on Feb 24!!! my fist patch MTF!!!!!
; projects[views_bootstrap_responsive_grid][patch][] = patches/views_bootstrap_responsive_grid-unsetnoticefix-2456873.patch

; so you can have equalheigh on each columns
projects[equalheights][version] = "2.4"

projects[special_menu_items][version] = "2.0"
projects[jquery_update][version] = "2.7"
projects[block_class][version] = "2.3"

; configure and set on next sprint
projects[path_breadcrumbs][version] = "3.3"
projects[ds][version] = "2.14"


;projects[omega][version] = "4.4"
;projects[panels][version] = "3.5"
;projects[panels_everywhere][version] = "1.0-rc2"

; using the boostrap views module
; projects[views_responsive_grid][version] = "1.3"
projects[crumbs][version] = "2.3"
projects[clean_markup][version] = "2.9"
projects[fences][version] = "1.0"
projects[commerce_add_to_cart_confirmation][version] = "1.0-rc2"
projects[commerce_ajax_cart][version] = "1.0-beta3"


; === Language ===
; ================
projects[l10n_update][version] = "2.0"
projects[l10n_client][version] = "1.3"


; === Image managment (resize, cache, etc) ===
; ============================================
; Chequear toda esta sección!!!


; create image styles on upload
projects[imageinfo_cache][version] = "3.5"
projects[imagefield_focus][version] = "1.x-dev"
projects[smartcrop][version] = "1.x-dev"
projects[imagefield_focus][version] = "1.x-dev"
projects[imagezoom][version] = "2.0-beta2"

; Image Formatter link to image style
projects[image_formatter_link_to_image_style][version] = "1.1"

; Image Cache Actions
projects[imagecache_actions][version] = "1.7"
; commited at 2016/01/10 MDF https://www.drupal.org/node/760438#comment-10733734
; projects[imagecache_actions][patch][] = patches/resize_overlay-760438-22.patch

; Token field path (images)
projects[filefield_paths][version] = "1.0"



projects[adaptive_image][version] = "1.4"

; For a coming sprint
; == ↓↓↓ Kameleoon (for a/b testing) ==
; projects[kameleoon][version] = "1.1"
; Nosto (recomendation engine)
; projects[commerce_nosto_tagging][version] = "1.0"

; Estoy usando esta wea? yo creo que no
; Sí, en: Base Store y Base Semillas, pero para algo?
; Sandbox module: UUID Features - Menu
;projects[uuid_features___menu][type] = module
;projects[uuid_features___menu][download][type] = git
;projects[uuid_features___menu][download][branch] = "7.x-1.x"
;projects[uuid_features___menu][download][url] = git://git.drupal.org/sandbox/ioskevich/2163429.git
;rojects[uuid_features___menu][download][revision] = 124b3c5739e1e476cac82313a6e1f7f647850066

; ==================
; ==== Checkout ====
; ==================

projects[commerce_checkout_progress][version] = "1.4"
projects[commerce_checkout_redirect][version] = "2.0-rc1"

; === Shipping ===
; ================

projects[commerce_flat_rate][version] = "1.0-beta2"
projects[addressfield_cl][version] = "1.0-beta1"
projects[geocoder][version] = "1.2"
projects[geophp][version] = "1.7"
projects[geocoder_autocomplete][version] = "1.x-dev"
projects[commerce_pickup][version] = "1.x-dev"

; === Payment ===
; ===============
; === BTC ===
projects[commerce_bitpay][version] = "2.4"
libraries[php-bitpay-client][download][type] = "file"
; da el puto Notice: Constant VERSION already defined in require_once() (line 44 of libraries/php-client/bp_lib.php
;libraries[php-bitpay-client][download][url] = "https://github.com/bitpay/php-bitpay-client/archive/b76b91d97ef94d82e9cbf7f03b592b9fd1034463.zip"
;libraries[php-bitpay-client][download][url] = "https://github.com/bitpay/php-bitpay-client/archive/v1.zip"
libraries[php-bitpay-client][download][url] = "https://github.com/sescandell/bitpay-client/archive/1.0.zip"
libraries[php-bitpay-client][directory_name] = "php-client"
libraries[php-bitpay-client][type] = "library"

; === Khipu ===
projects[khipu][version] = "1.0"

libraries[Khipu][download][type] = "file"
libraries[Khipu][download][url] = "https://github.com/Tifon-/Khipu/archive/v1.2.zip"
libraries[Khipu][directory_name] = "Khipu"
libraries[Khipu][type] = "library"

projects[commerce_khipu][version] = "1.0-beta2"

; === transbank ===
; Algun día
; projects[webpay][version] = "1.2"
; projects[commerce_webpay][version] = "1.0-rc2"

; === User Registration ===
; =========================

projects[commerce_checkout_complete_registration][version] = "1.3"

; Para el facebú
; projects[oauth][version] = "3.2"
; projects[oauthconnector][version] = "1.0-beta2"


; ===============================
; === Product Import / Export ===
; ===============================

projects[feeds][version] = "2.0-beta2"
projects[feeds][patch][] = patches/feeds-image-field-status-2512824-7.patch
;projects[migrate][version] = "2.8"


projects[commerce_feeds][version] = "1.4"
projects[feeds_tamper][version] = "1.1"
projects[views_data_export][version] = "3.0-beta9"
projects[image_url_formatter][version] = "1.4"
projects[feeds_fetcher_directory][version] = "2.0-beta5"


; ===================
; == Devel & tools ==
; ===================

projects[devel][version] = "1.5"
projects[drupalforfirebug][version] = "1.4"
projects[advanced_help][version] = "1.3"
projects[diff][version] = "3.2"


; ===================
; ==== Marketing ====
; ===================

projects[affiliate_ng][version] = "1.x-dev"
projects[commerce_affiliate][version] = "1.x-dev"
projects[userpoints][version] = "1.1"

;projects[supercookie][version] = "1.5"



; ======================
; ======================
; ===== Gran zorra =====


; Entity Views Attachment (parece que lo necesita commerce)
projects[eva][version] = "1.3"

; Field Extractor
projects[field_extractor][version] = "1.3"

; http_client
projects[http_client][version] = "2.4"

; Inline Conditions
projects[inline_conditions][version] = "1.0-alpha5"

; Mailjet
projects[mailjet][version] = "2.4"

; Mailsystem
projects[mailsystem][version] = "2.34"

; Message
projects[message][version] = "1.12"

; Message Notify
projects[message_notify][version] = "2.5"

; Mimemail (html)
projects[mimemail][version] = "1.0-beta4"

; Remote Stream Wrapper (allow to set files http://blablabla )
projects[remote_stream_wrapper][version] = "1.0-rc1"

; Service Links
projects[service_links][version] = "2.3"

; View bulk operations
; Se usa en PICEC, parece que es requerido por commerce
projects[views_bulk_operations][version] = "3.3"

; View megarow
projects[views_megarow][version] = "1.6"

; Strongarm
projects[strongarm][version] = "2.0"

; UUID Features
projects[uuid_features][version] = "1.0-alpha4"

; UUID
projects[uuid][version] = "1.0-beta2"

; Product Schema
projects[commerce_goodrelations][version] = "1.0-alpha2"

; Five Stars
projects[fivestar][version] = "2.1"


; Job Scheduler
projects[job_scheduler][version] = "2.0-alpha3"


; To add
; For last visited products view
; projects[recently_read][version] = "3.1"
; For user pictures
; projects[userpickit][version] = "1.0-beta3"
