<?php
/**
 * Created by PhpStorm.
 * User: N
 * Date: 2/12/16
 * Time: 08:58
 */
// Check for default drupal password
$default_pass_file = getenv("HOME")."/.bancosemillas-dev.settings.php";
if(is_file($default_pass_file)){
    require_once($default_pass_file);
} else {
    echo "There is not default password".PHP_EOL;
    $default_pass = userPrompt('Set a default password:');
    $default_pass_file_content = '<?php $default_pass='.$default_pass.';';
    file_put_contents($default_pass_file, $default_pass_file_content);
}
// Get which host we'll init
$hosts['local']['active'] = true;
$hosts['local']['drush_alias'] = '@banco-semillas._local ';
$hosts['local']['host'] = 'http://bancosemillas.dd:8083';
$hosts['featurizer']['active'] = true;
$hosts['featurizer']['drush_alias'] = '@banco-semillas._local ';
$hosts['featurizer']['host'] = 'http://featurizer.dd:8083';
$hosts['platform']['active'] = true;
$hosts['platform']['drush_alias'] = '@banco-semillas.master ';
$hosts['platform']['host'] = 'http://master-vjpnmm3nww2ak.eu.platform.sh';
$hosts['platform']['ssh'] = 'vjpnmm3nww2ak-master@ssh.eu.platform.sh';

if(!$hosts[$argv[1]]['active']){
    echo $argv[1].' is not a valid host'.PHP_EOL;
    $host = userPrompt('Enter a valid host:', 'validate_host');
} else {
    $host = $argv[1];
}
//echo 'Default pass is now: '.$default_pass.PHP_EOL;
//echo 'Host is now: '.$host.PHP_EOL;

// Featurizer lock
$featurizer_lock_file = '.platform/local/shared/files/featurizer.lock';
if($host == 'featurizer'){
    touch($featurizer_lock_file);
}
$path_prefix = 'cd _www && ';

// Load platform command alias
//passthru('source ~/.bashrc');

//SQL Drop;
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'sql-drop');
// File removal
echo 'Removing styles, img and feeds files'.PHP_EOL;
if($host == 'platform') {
    passthru($path_prefix . "platform ssh 'rm -R ~/private/styles'");
    passthru($path_prefix . "platform ssh 'rm -R ~/private/img'");
    passthru($path_prefix . "platform ssh 'rm -R ~/private/feeds'");
    passthru($path_prefix . "platform ssh 'rm -R ~/private/icons'");
}elseif($host == 'local'){
    //passthru($path_prefix . 'cd .. && pwd');
    //exit;
    passthru('rm -R .platform/local/shared/private/styles');
    passthru('rm -R .platform/local/shared/private/img');
    passthru('rm -R .platform/local/shared/private/feeds');
    passthru('rm -R .platform/local/shared/private/icons');
}
// Set site name
if($host == 'platform'){
    $site_name = 'Banco\ Semillas';
} else {
    $site_name = mb_strtoupper($host).'\ Banco\ Semillas';
}

// Install
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias']."site-install minimal --sites-subdir=default --account-name=nahuel --account-pass=$default_pass --account-mail=nahuel@fastmail.com --site-name=$site_name --site-mail=web@bancosemillas.cl install_configure_form.site_default_country=CL install_configure_form.date_default_timezone=America\/Santiago install_configure_form.update_status_module=array\(true, false\) -y");
// Themes
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en adminimal -y'); # admin theme
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en bootstrap -y'); # base theme
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en banco_semillas_bss -y'); # custom theme
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'vset theme_default banco_semillas_bss'); # Set custom theme as default
# Features
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en base_store -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en base_semillas -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en shipping -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en payment -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en picec_home_page -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en picec_checkout -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en bs_seo -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en bs_theme_bootstrap -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en bs_wysiwyg -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en bs_analytics -y');
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en bs_recommender -y');
# Update module (just to keep an eye on updates here and there)
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en update -y');

// Dev modules
if($host == 'featurizer' or $host == 'local') {
    passthru($path_prefix.'drush en diff -y');
    passthru($path_prefix.'drush en devel -y');
} else {
    // Ops settings
    passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'vset cache 0 -y');
    passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'vset preprocess_css 0 -y');
    passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'vset preprocess_js 0 -y');
}

// Block disabling and SQL Injecting
if($host == 'local' or $host == 'featurizer'){
    passthru($path_prefix.'drush block-disable --module=system --delta=navigation --theme=adminimal');
    passthru($path_prefix.'drush block-disable --module=system --delta=management --theme=adminimal');
    passthru($path_prefix.'drush block-disable --module=system --delta=navigation --theme=banco_semillas_bss');
    passthru($path_prefix.'drush block-disable --module=system --delta=management --theme=banco_semillas_bss');
    passthru($path_prefix.'drush block-disable --module=user --delta=login --theme=banco_semillas_bss');
    echo "Running sql ducktaped: default image".PHP_EOL;
    passthru('cat sql/semilla_image_default.sql | drush '.$hosts[$host]['drush_alias'].'sql-cli');
    echo "Running sql ducktaped: khipu accounts".PHP_EOL;
    passthru('cat sql/khipu_accounts.sql | drush '.$hosts[$host]['drush_alias'].'sql-cli');
} elseif ($host == 'platform'){
    echo "Running sql ducktaped: blocks disable".PHP_EOL;
    passthru('cat sql/disable_blocks.sql | platform -y drush sql-cli');
    echo "Running sql ducktaped: default image".PHP_EOL;
    passthru('cat sql/semilla_image_default.sql | platform -y drush sql-cli');
    echo "Running sql ducktaped: khipu accounts".PHP_EOL;
    passthru('cat sql/khipu_accounts.sql | platform -y drush sql-cli');
}

// Temp fix: core issue, google: drupal private_default_image_fix
//
passthru($path_prefix.'drush '.$hosts[$host]['drush_alias'].'en private_default_image_fix -y');

// Platform base files
if($host == 'platform') {
    echo "Uploading default images" . PHP_EOL;
    passthru('rsync -v -r .platform/local/shares/private/default_images/. '.$hosts['platform']['ssh'].':private/default_images/');
    echo "Uploading import images" . PHP_EOL;
    passthru('rsync -v -r .platform/local/shares/private/import/. '.$hosts['platform']['ssh'].':private/import/');
}

// Local set private file path
if($host == 'local' or $host == 'featurizer'){
    passthru($path_prefix.'drush vset file_private_path "../../../shared/private"');
}

// Sound alert
passthru('afplay /System/Library/Sounds/Glass.aiff');

// We fucking DONE here
echo 'Go and run yo iim init now beatch!'.PHP_EOL;
echo imacros_s($default_pass, $hosts[$host]['host']).PHP_EOL.PHP_EOL;
echo $imacros_s.PHP_EOL;



// Remove featurizer lock
if($host == 'featurizer'){
    unlink($featurizer_lock_file);
}

// Functions
function validate_host($host){
    global $hosts;
    if($hosts[$host]['active'] === true){
        return true;
    } else {
        return false;
    }
}
function isCLI() {
    return (php_sapi_name() === 'cli' OR defined('STDIN'));
}

function userPrompt($message, $validator=null) {
    if (!isCLI()) return null;

    print($message);
    $handle = fopen ('php://stdin','r');
    $line = rtrim(fgets($handle), "\r\n");

    if (is_callable($validator) && !call_user_func($validator, $line)) {
        print("Invalid Entry.\r\n");
        return userPrompt($message, $validator);
    } else {
        print("Continuing...\r\n");
        return $line;
    }
}
function imacros_s($default_pass, $default_host)
{
    return "
VERSION BUILD=8961227 RECORDER=FX
TAB T=1
URL GOTO=$default_host/user/login
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:user-login ATTR=ID:edit-name CONTENT=nahuel
SET !ENCRYPTION NO
TAG POS=1 TYPE=INPUT:PASSWORD FORM=ID:user-login ATTR=ID:edit-pass CONTENT=$default_pass
TAG POS=1 TYPE=BUTTON FORM=ID:user-login ATTR=ID:edit-submit
URL GOTO=$default_host/import/seeds_breeders
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:feeds-import-form ATTR=ID:edit-feeds-feedsfilefetcher-source CONTENT=private://import/pd_seed_breeders.csv
TAG POS=1 TYPE=INPUT:SUBMIT FORM=ID:feeds-import-form ATTR=ID:edit-submit
WAIT SECONDS=10
URL GOTO=$default_host/import/seeds_activities
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:feeds-import-form ATTR=ID:edit-feeds-feedsfilefetcher-source CONTENT=private://import/pd_seed_activities.csv
TAG POS=1 TYPE=INPUT:SUBMIT FORM=ID:feeds-import-form ATTR=ID:edit-submit
WAIT SECONDS=10
URL GOTO=$default_host/import/seeds_medical
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:feeds-import-form ATTR=ID:edit-feeds-feedsfilefetcher-source CONTENT=private://import/pd_seed_medical.csv
TAG POS=1 TYPE=INPUT:SUBMIT FORM=ID:feeds-import-form ATTR=ID:edit-submit
WAIT SECONDS=10
URL GOTO=$default_host/import/seeds_flavors
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:feeds-import-form ATTR=ID:edit-feeds-feedsfilefetcher-source CONTENT=private://import/pd_seed_flavors.csv
TAG POS=1 TYPE=INPUT:SUBMIT FORM=ID:feeds-import-form ATTR=ID:edit-submit
WAIT SECONDS=10
URL GOTO=$default_host/import/semillas_pv
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:feeds-import-form ATTR=ID:edit-feeds-feedsfilefetcher-source CONTENT=private://import/semillas-pv-withSKU.csv
TAG POS=1 TYPE=INPUT:SUBMIT FORM=ID:feeds-import-form ATTR=ID:edit-submit
WAIT SECONDS=30
URL GOTO=$default_host/import/semillas_pd
TAG POS=1 TYPE=INPUT:TEXT FORM=ID:feeds-import-form ATTR=ID:edit-feeds-feedsfilefetcher-source CONTENT=private://import/semillas-pd-es.xml-joined.csv
TAG POS=1 TYPE=INPUT:SUBMIT FORM=ID:feeds-import-form ATTR=ID:edit-submit";
}