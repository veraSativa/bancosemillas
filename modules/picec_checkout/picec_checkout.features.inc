<?php
/**
 * @file
 * picec_checkout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function picec_checkout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
