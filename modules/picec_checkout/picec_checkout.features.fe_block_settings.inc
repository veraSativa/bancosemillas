<?php
/**
 * @file
 * picec_checkout.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function picec_checkout_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['commerce_checkout_progress-indication'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'indication',
    'module' => 'commerce_checkout_progress',
    'node_types' => array(),
    'pages' => 'checkout/*
cart',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => -10,
      ),
      'banco_semillas_bss' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'banco_semillas_bss',
        'weight' => -10,
      ),
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -10,
      ),
      'bootstrap' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bootstrap',
        'weight' => -10,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
