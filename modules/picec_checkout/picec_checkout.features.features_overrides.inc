<?php
/**
 * @file
 * picec_checkout.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function picec_checkout_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: views_view
  $overrides["views_view.commerce_cart_form.display|default|display_options|fields|commerce_total|type"]["DELETED"] = TRUE;
  $overrides["views_view.commerce_cart_form.display|default|display_options|fields|commerce_unit_price|type"]["DELETED"] = TRUE;
  $overrides["views_view.commerce_cart_form.display|default|display_options|fields|field_image"] = array(
    'id' => 'field_image',
    'table' => 'field_data_field_image',
    'field' => 'field_image',
    'relationship' => 'field_product',
    'label' => '',
    'element_label_colon' => FALSE,
    'click_sort_column' => 'fid',
    'settings' => array(
      'image_style' => 'bs_thumbnail',
      'image_link' => 'content',
    ),
    'delta_limit' => 1,
    'delta_offset' => 0,
    'separator' => '',
  );
  $overrides["views_view.commerce_cart_form.display|default|display_options|query|options|disable_sql_rewrite"] = TRUE;
  $overrides["views_view.commerce_cart_form.display|default|display_options|query|options|query_comment"]["DELETED"] = TRUE;
  $overrides["views_view.commerce_cart_form.display|default|display_options|relationships|commerce_product_product_id_1"] = array(
    'id' => 'commerce_product_product_id_1',
    'table' => 'field_data_commerce_product',
    'field' => 'commerce_product_product_id',
    'relationship' => 'commerce_line_items_line_item_id',
    'required' => TRUE,
  );
  $overrides["views_view.commerce_cart_form.display|default|display_options|relationships|field_product"] = array(
    'id' => 'field_product',
    'table' => 'commerce_product',
    'field' => 'field_product',
    'relationship' => 'commerce_product_product_id_1',
    'required' => TRUE,
  );
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|columns|field_image"] = 'line_item_title';
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|commerce_display_path|empty_column"] = 0;
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|commerce_total|empty_column"] = 0;
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|commerce_unit_price|empty_column"] = 0;
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|edit_delete|empty_column"] = 0;
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|edit_quantity|empty_column"] = 0;
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|field_image"] = array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  );
  $overrides["views_view.commerce_cart_form.display|default|display_options|style_options|info|line_item_title|empty_column"] = 0;

 return $overrides;
}
