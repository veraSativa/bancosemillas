<?php
/**
 * @file
 * bs_theme_bootstrap.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function bs_theme_bootstrap_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product_display_semilla|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product_display_semilla';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
  );
  $export['node|product_display_semilla|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function bs_theme_bootstrap_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_display_semilla|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_display_semilla';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
      ),
      'right' => array(
        1 => 'title',
        2 => 'product:commerce_price',
        3 => 'field_product',
        4 => 'product:field_sex',
        5 => 'field_flowering',
        6 => 'field_flowering_time',
        7 => 'field_heritage',
        8 => 'field_flavors',
        9 => 'field_thc',
        10 => 'field_cbd',
        11 => 'field_height',
        12 => 'field_harvest_m2',
        13 => 'field_harvest_plant',
      ),
      'footer' => array(
        14 => 'body',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'title' => 'right',
      'product:commerce_price' => 'right',
      'field_product' => 'right',
      'product:field_sex' => 'right',
      'field_flowering' => 'right',
      'field_flowering_time' => 'right',
      'field_heritage' => 'right',
      'field_flavors' => 'right',
      'field_thc' => 'right',
      'field_cbd' => 'right',
      'field_height' => 'right',
      'field_harvest_m2' => 'right',
      'field_harvest_plant' => 'right',
      'body' => 'footer',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'header' => array(
        'col-sm-12' => 'col-sm-12',
      ),
      'left' => array(
        'col-md-8' => 'col-md-8',
      ),
      'right' => array(
        'col-md-4' => 'col-md-4',
        'panel' => 'panel',
      ),
      'footer' => array(
        'col-sm-12' => 'col-sm-12',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|product_display_semilla|full'] = $ds_layout;

  return $export;
}
