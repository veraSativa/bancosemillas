<?php
/**
 * @file
 * bs_theme_bootstrap.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function bs_theme_bootstrap_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-bs-footer-menu.
  $menus['menu-bs-footer-menu'] = array(
    'menu_name' => 'menu-bs-footer-menu',
    'title' => 'BS Footer menu',
    'description' => '',
  );
  // Exported menu: menu-bs-header-bottom-menu.
  $menus['menu-bs-header-bottom-menu'] = array(
    'menu_name' => 'menu-bs-header-bottom-menu',
    'title' => 'BS Header bottom menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('BS Footer menu');
  t('BS Header bottom menu');

  return $menus;
}
