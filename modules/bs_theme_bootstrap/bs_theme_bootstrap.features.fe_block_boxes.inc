<?php
/**
 * @file
 * bs_theme_bootstrap.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function bs_theme_bootstrap_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Copyleft, powered by, etc.';
  $fe_block_boxes->format = 'markdown';
  $fe_block_boxes->machine_name = 'copywrite';
  $fe_block_boxes->body = 'Copyleft 2016, Alto Vuelo SpA, Chile. Algunos derechos reservados.
[Orgullosamente](http://nahuel.me/drupal) propulsado por [Drupal](http://drupalchile.org) 7.';

  $export['copywrite'] = $fe_block_boxes;

  return $export;
}
