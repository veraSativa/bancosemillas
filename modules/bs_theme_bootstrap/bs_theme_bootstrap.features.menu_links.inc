<?php
/**
 * @file
 * bs_theme_bootstrap.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bs_theme_bootstrap_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-bs-footer-menu_acerca-de:<front>.
  $menu_links['menu-bs-footer-menu_acerca-de:<front>'] = array(
    'menu_name' => 'menu-bs-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Acerca de',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-footer-menu_acerca-de:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-bs-footer-menu_cambios-devoluciones-y-garanta:<front>.
  $menu_links['menu-bs-footer-menu_cambios-devoluciones-y-garanta:<front>'] = array(
    'menu_name' => 'menu-bs-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Cambios, devoluciones y garantía',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-footer-menu_cambios-devoluciones-y-garanta:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-bs-footer-menu_poltica-de-privacidad:<front>.
  $menu_links['menu-bs-footer-menu_poltica-de-privacidad:<front>'] = array(
    'menu_name' => 'menu-bs-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Política de privacidad',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-footer-menu_poltica-de-privacidad:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-bs-footer-menu_preguntas-frecuentes:<front>.
  $menu_links['menu-bs-footer-menu_preguntas-frecuentes:<front>'] = array(
    'menu_name' => 'menu-bs-footer-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Preguntas frecuentes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-footer-menu_preguntas-frecuentes:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-bs-header-bottom-menu_contacto:<front>.
  $menu_links['menu-bs-header-bottom-menu_contacto:<front>'] = array(
    'menu_name' => 'menu-bs-header-bottom-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Contacto',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-header-bottom-menu_contacto:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-bs-header-bottom-menu_recomendador:<front>.
  $menu_links['menu-bs-header-bottom-menu_recomendador:<front>'] = array(
    'menu_name' => 'menu-bs-header-bottom-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Recomendador',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-header-bottom-menu_recomendador:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-bs-header-bottom-menu_semillas-inicio:<front>.
  $menu_links['menu-bs-header-bottom-menu_semillas-inicio:<front>'] = array(
    'menu_name' => 'menu-bs-header-bottom-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Semillas (Inicio)',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-bs-header-bottom-menu_semillas-inicio:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Acerca de');
  t('Cambios, devoluciones y garantía');
  t('Contacto');
  t('Política de privacidad');
  t('Preguntas frecuentes');
  t('Recomendador');
  t('Semillas (Inicio)');

  return $menu_links;
}
