<?php
/**
 * @file
 * bs_theme_bootstrap.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function bs_theme_bootstrap_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-copywrite'] = array(
    'cache' => -1,
    'css_class' => 'col-sm-6',
    'custom' => 0,
    'machine_name' => 'copywrite',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas_bss' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'banco_semillas_bss',
        'weight' => -9,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-bs-footer-menu'] = array(
    'cache' => -1,
    'css_class' => 'col-sm-6',
    'custom' => 0,
    'delta' => 'menu-bs-footer-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas_bss' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'banco_semillas_bss',
        'weight' => -10,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['menu-menu-bs-header-bottom-menu'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'menu-bs-header-bottom-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas_bss' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'banco_semillas_bss',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
