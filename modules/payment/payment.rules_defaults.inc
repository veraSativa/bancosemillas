<?php
/**
 * @file
 * payment.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function payment_default_rules_configuration() {
  $items = array();
  $items['commerce_payment_bitpay_bs'] = entity_import('rules_config', '{ "commerce_payment_bitpay_bs" : {
      "LABEL" : "BitPay BS",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Payment" ],
      "REQUIRES" : [ "commerce_payment" ],
      "ON" : { "commerce_payment_methods" : [] },
      "DO" : [
        { "commerce_payment_enable_bitpay" : {
            "commerce_order" : [ "commerce-order" ],
            "payment_method" : { "value" : {
                "method_id" : "bitpay",
                "settings" : {
                  "apiKey" : "O9UVnJTEekHZ0hmeH6tR7Ucwg1LunVht8jytvBdI",
                  "checkout_display" : "both",
                  "redirectURL" : "",
                  "notificationEmail" : "pagos@bancosemillas.cl",
                  "transactionSpeed" : "",
                  "fullNotifications" : "true",
                  "redirect_mode" : "iframe"
                }
              }
            }
          }
        }
      ]
    }
  }');
  $items['commerce_payment_khipu_bs'] = entity_import('rules_config', '{ "commerce_payment_khipu_bs" : {
      "LABEL" : "Khipu BS",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Payment" ],
      "REQUIRES" : [ "commerce_payment" ],
      "ON" : { "commerce_payment_methods" : [] },
      "DO" : [
        { "commerce_payment_enable_khipu" : {
            "commerce_order" : [ "commerce-order" ],
            "payment_method" : { "value" : {
                "method_id" : "khipu",
                "settings" : {
                  "kaid" : "1",
                  "subject" : "este es el texto de subjet",
                  "body" : "este es el texto de body",
                  "picture_url" : "",
                  "button_text" : "Click to be redirected manually to Khipu"
                }
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
