<?php
/**
 * @file
 * base_store.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function base_store_taxonomy_default_vocabularies() {
  return array(
    'fabricante' => array(
      'name' => 'Fabricante',
      'machine_name' => 'fabricante',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
