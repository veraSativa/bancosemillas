<?php
/**
 * @file
 * base_store.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function base_store_user_default_roles() {
  $roles = array();

  // Exported role: blogger.
  $roles['blogger'] = array(
    'name' => 'blogger',
    'weight' => 6,
  );

  // Exported role: content editor.
  $roles['content editor'] = array(
    'name' => 'content editor',
    'weight' => 5,
  );

  // Exported role: customer.
  $roles['customer'] = array(
    'name' => 'customer',
    'weight' => 7,
  );

  // Exported role: store manager.
  $roles['store manager'] = array(
    'name' => 'store manager',
    'weight' => 3,
  );

  // Exported role: store operator.
  $roles['store operator'] = array(
    'name' => 'store operator',
    'weight' => 4,
  );

  return $roles;
}
