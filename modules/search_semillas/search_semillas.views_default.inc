<?php
/**
 * @file
 * search_semillas.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function search_semillas_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'busqueda_pd';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_index_pd';
  $view->human_name = 'Busqueda PD';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Busqueda Productos';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '40';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['columns'] = '5';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Indexed Node: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_index_pd';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Node: Imagen */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'search_api_index_index_pd';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'bs_thumbnail',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_image']['delta_offset'] = '0';
  /* Field: Indexed Node: Banco de Origen */
  $handler->display->display_options['fields']['field_banco_origen']['id'] = 'field_banco_origen';
  $handler->display->display_options['fields']['field_banco_origen']['table'] = 'search_api_index_index_pd';
  $handler->display->display_options['fields']['field_banco_origen']['field'] = 'field_banco_origen';
  /* Field: Product variations » Price: Amount (indexed) */
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['id'] = 'field_product_commerce_price_amount';
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['table'] = 'search_api_index_index_pd';
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['field'] = 'field_product_commerce_price_amount';
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['label'] = '';
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['precision'] = '0';
  $handler->display->display_options['fields']['field_product_commerce_price_amount']['link_to_entity'] = 0;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_index_pd';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Fulltext search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'b';
  $export['busqueda_pd'] = $view;

  return $export;
}
