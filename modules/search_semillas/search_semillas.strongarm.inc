<?php
/**
 * @file
 * search_semillas.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function search_semillas_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi_pretty_paths_searcher_search_api@index_pd';
  $strongarm->value = 1;
  $export['facetapi_pretty_paths_searcher_search_api@index_pd'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'facetapi_pretty_paths_searcher_search_api@index_pd_options';
  $strongarm->value = array(
    'sort_path_segments' => 0,
    'base_path_provider' => 'default',
  );
  $export['facetapi_pretty_paths_searcher_search_api@index_pd_options'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'search_api_facets_search_ids';
  $strongarm->value = array(
    'index_pd' => array(
      'search_api_views:busqueda_semillas:page' => 'search_api_views:busqueda_semillas:page',
      'search_api_views:busqueda_pd:page' => 'search_api_views:busqueda_pd:page',
    ),
  );
  $export['search_api_facets_search_ids'] = $strongarm;

  return $export;
}
