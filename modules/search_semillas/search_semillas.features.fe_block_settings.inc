<?php
/**
 * @file
 * search_semillas.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function search_semillas_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['facetapi-8ELAtiZuTgTXqi1X8PfBSYVWKerdZ3L2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '8ELAtiZuTgTXqi1X8PfBSYVWKerdZ3L2',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'banco_semillas',
        'weight' => -10,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
    ),
    'title' => 'Presentación',
    'visibility' => 0,
  );

  $export['facetapi-EEtwhc94GB8D437Eo50Vg9kc1g19P1I7'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'EEtwhc94GB8D437Eo50Vg9kc1g19P1I7',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'banco_semillas',
        'weight' => -8,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
    ),
    'title' => 'Sexo',
    'visibility' => 0,
  );

  $export['facetapi-Up51gFiHbSxA4ruo1115XLlYNHYbK23H'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'Up51gFiHbSxA4ruo1115XLlYNHYbK23H',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'banco_semillas',
        'weight' => -11,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
    ),
    'title' => 'Genética',
    'visibility' => 0,
  );

  $export['facetapi-X0OAi1myJr8Qq6L0nVXkchK9HHACBnGF'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'X0OAi1myJr8Qq6L0nVXkchK9HHACBnGF',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'banco_semillas',
        'weight' => -7,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
    ),
    'title' => 'Criador',
    'visibility' => 0,
  );

  $export['facetapi-qOsitBYiI1eFGarJevdkgNO414quhLMx'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'qOsitBYiI1eFGarJevdkgNO414quhLMx',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'banco_semillas',
        'weight' => -9,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
    ),
    'title' => 'Precio',
    'visibility' => 0,
  );

  $export['facetapi-rqH51de3z4uos5CpeJS0P1XPdOJ31bDx'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'rqH51de3z4uos5CpeJS0P1XPdOJ31bDx',
    'module' => 'facetapi',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'banco_semillas',
        'weight' => -12,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
    ),
    'title' => 'Floración',
    'visibility' => 0,
  );

  return $export;
}
