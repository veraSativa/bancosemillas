<?php
/**
 * @file
 * search_semillas.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function search_semillas_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function search_semillas_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function search_semillas_default_search_api_index() {
  $items = array();
  $items['index_pd'] = entity_import('search_api_index', '{
    "name" : "Index pd",
    "machine_name" : "index_pd",
    "description" : null,
    "server" : "platform_default",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "product_display_semilla" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "body:value" : { "type" : "text" },
        "field_banco_origen" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_banco_origen:name" : { "type" : "text", "boost" : "2.0" },
        "field_banco_origen:url" : { "type" : "uri" },
        "field_flowering" : { "type" : "boolean" },
        "field_flowering_time:from" : { "type" : "integer" },
        "field_flowering_time:to" : { "type" : "integer" },
        "field_heritage" : { "type" : "integer" },
        "field_product" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "commerce_product" },
        "field_product:commerce_price:amount" : { "type" : "list\\u003Cdecimal\\u003E" },
        "field_product:field_package" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product:field_sex" : { "type" : "list\\u003Cboolean\\u003E" },
        "field_product:sku" : { "type" : "list\\u003Ctext\\u003E", "boost" : "3.0" },
        "search_api_language" : { "type" : "string" },
        "search_api_url" : { "type" : "uri" },
        "title" : { "type" : "text", "boost" : "5.0" },
        "url" : { "type" : "uri" }
      },
      "data_alter_callbacks" : {
        "commerce_search_api_alter_product_status" : { "status" : 1, "weight" : "-9", "settings" : [] },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "title" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function search_semillas_default_search_api_server() {
  $items = array();
  $items['platform_default'] = entity_import('search_api_server', '{
    "name" : "Platform Default",
    "machine_name" : "platform_default",
    "description" : "",
    "class" : "search_api_solr_service",
    "options" : {
      "clean_ids" : true,
      "site_hash" : true,
      "scheme" : "http",
      "host" : "solr.internal",
      "port" : "8080",
      "path" : "\\/solr",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "skip_schema_check" : 0,
      "solr_version" : "",
      "http_method" : "AUTO",
      "autocorrect_spell" : 1,
      "autocorrect_suggest_words" : 1
    },
    "enabled" : "1"
  }');
  return $items;
}
