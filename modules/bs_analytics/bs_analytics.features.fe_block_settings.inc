<?php
/**
 * @file
 * bs_analytics.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function bs_analytics_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['piwik_reports-piwik_page_report'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'piwik_page_report',
    'module' => 'piwik_reports',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'dashboard_main',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'banco_semillas_bss' => array(
        'region' => '',
        'status' => 1,
        'theme' => 'banco_semillas_bss',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'bootstrap' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bootstrap',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
