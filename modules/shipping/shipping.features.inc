<?php
/**
 * @file
 * shipping.features.inc
 */

/**
 * Implements hook_commerce_flat_rate_default_services().
 */
function shipping_commerce_flat_rate_default_services() {
  $items = array(
    'chilexpress_dc' => array(
      'name' => 'chilexpress_dc',
      'base' => 'chilexpress_dc',
      'display_title' => 'Despacho Rápido',
      'description' => 'Las compras realizadas hasta las 17:00, llegarán a tu domicilio el siguiente día hábil.',
      'shipping_method' => 'flat_rate',
      'rules_component' => TRUE,
      'price_component' => 'flat_rate_chilexpress_dc',
      'weight' => 0,
      'callbacks' => array(
        'rate' => 'commerce_flat_rate_service_rate_order',
        'details_form' => 'chilexpress_dc_details_form',
        'details_form_validate' => 'chilexpress_dc_details_form_validate',
        'details_form_submit' => 'chilexpress_dc_details_form_submit',
      ),
      'module' => 'commerce_flat_rate',
      'title' => 'Chilexpress documento nacional pequeño',
      'base_rate' => array(
        'amount' => 2320,
        'currency_code' => 'CLP',
        'data' => array(
          'components' => array(
            0 => array(
              'name' => 'flat_rate_chilexpress_dc',
              'price' => array(
                'amount' => 1950,
                'currency_code' => 'CLP',
                'data' => array(),
              ),
              'included' => TRUE,
            ),
            1 => array(
              'name' => 'tax|iva_chile',
              'price' => array(
                'amount' => 370,
                'currency_code' => 'CLP',
                'data' => array(
                  'tax_rate' => array(
                    'name' => 'iva_chile',
                    'display_title' => 'IVA (Chile)',
                    'description' => '',
                    'rate' => 0.19,
                    'type' => 'vat',
                    'rules_component' => 'commerce_tax_rate_iva_chile',
                    'default_rules_component' => 1,
                    'price_component' => 'tax|iva_chile',
                    'calculation_callback' => 'commerce_tax_rate_calculate',
                    'module' => 'commerce_tax_ui',
                    'title' => 'IVA (Chile)',
                    'admin_list' => TRUE,
                  ),
                ),
              ),
              'included' => TRUE,
            ),
          ),
        ),
      ),
      'data' => array(
        'include_tax' => 'iva_chile',
      ),
      'admin_list' => TRUE,
      'is_features' => TRUE,
    ),
  );
  return $items;
}
