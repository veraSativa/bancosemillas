<?php
/**
 * @file
 * shipping.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function shipping_default_rules_configuration() {
  $items = array();
  $items['commerce_shipping_service_chilexpress_dc'] = entity_import('rules_config', '{ "commerce_shipping_service_chilexpress_dc" : {
      "LABEL" : "Rate Chilexpress documento nacional peque\\u00f1o",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_shipping" ],
      "USES VARIABLES" : { "commerce_order" : { "type" : "commerce_order", "label" : "Order" } },
      "DO" : [
        { "commerce_shipping_service_rate_order" : {
            "shipping_service_name" : "chilexpress_dc",
            "commerce_order" : [ "commerce-order" ]
          }
        }
      ]
    }
  }');
  $items['rules_zonas_no_centrales'] = entity_import('rules_config', '{ "rules_zonas_no_centrales" : {
      "LABEL" : "Zonas no centrales",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "commerce_order", "commerce_line_item", "rules", "commerce_shipping" ],
      "ON" : { "commerce_shipping_calculate_rate" : [] },
      "IF" : [
        { "commerce_order_compare_address" : {
            "commerce_order" : [ "commerce-line-item:order" ],
            "address_field" : "commerce_customer_shipping|commerce_customer_address",
            "address_component" : "dependent_locality",
            "operator" : "is one of",
            "value" : "\\r\\n15102\\r\\n15202\\r\\n15201\\r\\n01107\\r\\n01101\\r\\n01402\\r\\n01403\\r\\n01404\\r\\n01405\\r\\n01401\\r\\n02101\\r\\n02102\\r\\n02103\\r\\n02104\\r\\n02201\\r\\n02202\\r\\n02203\\r\\n02302\\r\\n02301\\r\\n03201\\r\\n03202\\r\\n10202\\r\\n10201\\r\\n10203\\r\\n10204\\r\\n10205\\r\\n10206\\r\\n10207\\r\\n10208\\r\\n10209\\r\\n10210\\r\\n10401\\r\\n10402\\r\\n10403\\r\\n10404\\r\\n11201\\r\\n11202\\r\\n11203\\r\\n11301\\r\\n11302\\r\\n11303\\r\\n11101\\r\\n11102\\r\\n11401\\r\\n11402\\r\\n12202\\r\\n12201\\r\\n12102\\r\\n12101\\r\\n12103\\r\\n12104\\r\\n12301\\r\\n12302\\r\\n12303\\r\\n12401\\r\\n12402"
          }
        }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_add" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : "2210",
            "component_name" : "flat_rate_chilexpress_dc",
            "round_mode" : "1"
          }
        },
        { "drupal_message" : { "message" : "Estamos ejecutando la regla \\u0022zonas no centrales\\u0022" } }
      ]
    }
  }');
  return $items;
}
