<?php
/**
 * @file
 * bs_seo.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function bs_seo_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_page_pattern';
  $strongarm->value = '[node:title]';
  $export['pathauto_node_page_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_product_display_semilla_pattern';
  $strongarm->value = '[node:field-ref-pd-seed-breeder:title]/[node:title]';
  $export['pathauto_node_product_display_semilla_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_fabricante_pattern';
  $strongarm->value = '[term:name]';
  $export['pathauto_taxonomy_term_fabricante_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_sabores_pattern';
  $strongarm->value = 'sabor/[term:name]';
  $export['pathauto_taxonomy_term_sabores_pattern'] = $strongarm;

  return $export;
}
