<?php
/**
 * @file
 * base_semillas.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function base_semillas_taxonomy_default_vocabularies() {
  return array(
    'sabores' => array(
      'name' => 'Sabores',
      'machine_name' => 'sabores',
      'description' => 'Referencias de sabores',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
