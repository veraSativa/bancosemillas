<?php
/**
 * @file
 * base_semillas.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function base_semillas_commerce_product_default_types() {
  $items = array(
    'semillas' => array(
      'type' => 'semillas',
      'name' => 'Semillas',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function base_semillas_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function base_semillas_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function base_semillas_node_info() {
  $items = array(
    'pd_seed_activity' => array(
      'name' => t('Semillas: Actividad o Estado de ánimo'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pd_seed_breeder' => array(
      'name' => t('Semillas: Obtentor'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pd_seed_flavor' => array(
      'name' => t('Semillas: Sabores'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'pd_seed_medical' => array(
      'name' => t('Semillas: Usos medicinales'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_display_semilla' => array(
      'name' => t('Semilla'),
      'base' => 'node_content',
      'description' => t('Product display de semillas'),
      'has_title' => '1',
      'title_label' => t('Titulo'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
