<?php
/**
 * @file
 * base_semillas.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function base_semillas_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create commerce_product entities of bundle semillas'.
  $permissions['create commerce_product entities of bundle semillas'] = array(
    'name' => 'create commerce_product entities of bundle semillas',
    'roles' => array(),
    'module' => 'commerce_product',
  );

  // Exported permission: 'create product_display_semilla content'.
  $permissions['create product_display_semilla content'] = array(
    'name' => 'create product_display_semilla content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any product_display_semilla content'.
  $permissions['delete any product_display_semilla content'] = array(
    'name' => 'delete any product_display_semilla content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own product_display_semilla content'.
  $permissions['delete own product_display_semilla content'] = array(
    'name' => 'delete own product_display_semilla content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any commerce_product entity of bundle semillas'.
  $permissions['edit any commerce_product entity of bundle semillas'] = array(
    'name' => 'edit any commerce_product entity of bundle semillas',
    'roles' => array(),
    'module' => 'commerce_product',
  );

  // Exported permission: 'edit any product_display_semilla content'.
  $permissions['edit any product_display_semilla content'] = array(
    'name' => 'edit any product_display_semilla content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own commerce_product entities of bundle semillas'.
  $permissions['edit own commerce_product entities of bundle semillas'] = array(
    'name' => 'edit own commerce_product entities of bundle semillas',
    'roles' => array(),
    'module' => 'commerce_product',
  );

  // Exported permission: 'edit own product_display_semilla content'.
  $permissions['edit own product_display_semilla content'] = array(
    'name' => 'edit own product_display_semilla content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'view any commerce_product entity of bundle semillas'.
  $permissions['view any commerce_product entity of bundle semillas'] = array(
    'name' => 'view any commerce_product entity of bundle semillas',
    'roles' => array(),
    'module' => 'commerce_product',
  );

  // Exported permission: 'view own commerce_product entities of bundle semillas'.
  $permissions['view own commerce_product entities of bundle semillas'] = array(
    'name' => 'view own commerce_product entities of bundle semillas',
    'roles' => array(),
    'module' => 'commerce_product',
  );

  return $permissions;
}
