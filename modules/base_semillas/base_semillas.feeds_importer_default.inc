<?php
/**
 * @file
 * base_semillas.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function base_semillas_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'seeds_activities';
  $feeds_importer->config = array(
    'name' => '0.2 Semillas: Actividades o Estados de ánimo',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Published',
            'target' => 'status',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'Icon',
            'target' => 'field_icon:uri',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Body',
            'target' => 'body',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'markdown',
        'skip_hash_check' => 0,
        'bundle' => 'pd_seed_activity',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['seeds_activities'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'seeds_breeders';
  $feeds_importer->config = array(
    'name' => '0.1 Semillas: Obtentor',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Published',
            'target' => 'status',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'Icon',
            'target' => 'field_icon:uri',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'WebSite',
            'target' => 'field_website:url',
            'unique' => FALSE,
            'language' => 'und',
          ),
          5 => array(
            'source' => 'Body',
            'target' => 'body',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'markdown',
        'skip_hash_check' => 0,
        'bundle' => 'pd_seed_breeder',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['seeds_breeders'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'seeds_flavors';
  $feeds_importer->config = array(
    'name' => '0.4 Semillas: Sabores',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Published',
            'target' => 'status',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'Icon',
            'target' => 'field_icon:uri',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Body',
            'target' => 'body',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'markdown',
        'skip_hash_check' => 0,
        'bundle' => 'pd_seed_flavor',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['seeds_flavors'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'seeds_medical';
  $feeds_importer->config = array(
    'name' => '0.3 Semillas: Usos Medicinales',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Published',
            'target' => 'status',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'Icon',
            'target' => 'field_icon:uri',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Body',
            'target' => 'body',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'markdown',
        'skip_hash_check' => 0,
        'bundle' => 'pd_seed_medical',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['seeds_medical'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'semillas_pd';
  $feeds_importer->config = array(
    'name' => '2. Semillas product display',
    'description' => 'Importador para "Product Display" de semillas',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
        'encoding' => 'UTF-8',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'PublishedStatus',
            'target' => 'status',
            'unique' => FALSE,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'PublishedFP',
            'target' => 'promote',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'PublishedSticky',
            'target' => 'sticky',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'PublishedDate',
            'target' => 'created',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 1,
            'language' => 'und',
          ),
          5 => array(
            'source' => 'Breeder',
            'target' => 'field_ref_pd_seed_breeder:etid',
            'unique' => FALSE,
            'language' => 'und',
          ),
          6 => array(
            'source' => 'Medical',
            'target' => 'field_ref_pd_seed_medical:etid',
            'unique' => FALSE,
            'language' => 'und',
          ),
          7 => array(
            'source' => 'Activity',
            'target' => 'field_ref_pd_seed_activity:etid',
            'unique' => FALSE,
            'language' => 'und',
          ),
          8 => array(
            'source' => 'Flavor',
            'target' => 'field_ref_pd_seed_flavor:etid',
            'unique' => FALSE,
            'language' => 'und',
          ),
          9 => array(
            'source' => 'Floracion',
            'target' => 'field_flowering',
            'unique' => FALSE,
            'language' => 'und',
          ),
          10 => array(
            'source' => 'Herencia',
            'target' => 'field_heritage',
            'unique' => FALSE,
            'language' => 'und',
          ),
          11 => array(
            'source' => 'THC',
            'target' => 'field_thc',
            'unique' => FALSE,
            'language' => 'und',
          ),
          12 => array(
            'source' => 'CBD',
            'target' => 'field_cbd',
            'unique' => FALSE,
            'language' => 'und',
          ),
          13 => array(
            'source' => 'RecommendedExpertice',
            'target' => 'field_recommended_expertice',
            'unique' => FALSE,
            'language' => 'und',
          ),
          14 => array(
            'source' => 'ProductivityLevel',
            'target' => 'field_productivity_level',
            'unique' => FALSE,
            'language' => 'und',
          ),
          15 => array(
            'source' => 'RecommendedEnv',
            'target' => 'field_recommended_env',
            'unique' => FALSE,
            'language' => 'und',
          ),
          16 => array(
            'source' => 'PeriodoFrom',
            'target' => 'field_flowering_time:from',
            'unique' => FALSE,
            'language' => 'und',
          ),
          17 => array(
            'source' => 'PeriodoTo',
            'target' => 'field_flowering_time:to',
            'unique' => FALSE,
            'language' => 'und',
          ),
          18 => array(
            'source' => 'HeightFrom',
            'target' => 'field_height:from',
            'unique' => FALSE,
            'language' => 'und',
          ),
          19 => array(
            'source' => 'HeightTo',
            'target' => 'field_height:to',
            'unique' => FALSE,
            'language' => 'und',
          ),
          20 => array(
            'source' => 'Harvest_m2From',
            'target' => 'field_harvest_m2:from',
            'unique' => FALSE,
            'language' => 'und',
          ),
          21 => array(
            'source' => 'Harvest_m2To',
            'target' => 'field_harvest_m2:to',
            'unique' => FALSE,
            'language' => 'und',
          ),
          22 => array(
            'source' => 'Harvest_plantFrom',
            'target' => 'field_harvest_plant:from',
            'unique' => FALSE,
            'language' => 'und',
          ),
          23 => array(
            'source' => 'Harvest_plantTo',
            'target' => 'field_harvest_plant:to',
            'unique' => FALSE,
            'language' => 'und',
          ),
          24 => array(
            'source' => 'Imagen',
            'target' => 'field_image:uri',
            'unique' => FALSE,
            'language' => 'und',
          ),
          25 => array(
            'source' => 'PV-SKU',
            'target' => 'field_product:sku',
            'unique' => FALSE,
            'language' => 'und',
          ),
          26 => array(
            'source' => 'Body',
            'target' => 'body',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'markdown',
        'authorize' => 1,
        'update_non_existent' => 'skip',
        'skip_hash_check' => 0,
        'bundle' => 'product_display_semilla',
        'insert_new' => '1',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['semillas_pd'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'semillas_pv';
  $feeds_importer->config = array(
    'name' => '1. Semillas product variations',
    'description' => 'Importador para "Product Variations" de semillas',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
        'encoding' => 'UTF-8',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsCommerceProductProcessor',
      'config' => array(
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'Presentacion',
            'target' => 'field_package',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Price',
            'target' => 'commerce_price:amount',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'SKU',
            'target' => 'sku',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'Cost',
            'target' => 'field_cost:amount',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Status',
            'target' => 'status',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Sexo',
            'target' => 'field_sex',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'product_type' => 'semillas',
        'tax_rate' => TRUE,
        'update_non_existent' => 'skip',
        'skip_hash_check' => 0,
        'bundle' => 'semillas',
        'insert_new' => '1',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['semillas_pv'] = $feeds_importer;

  return $export;
}
