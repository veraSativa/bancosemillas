<?php
/**
 * @file
 * base_semillas.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function base_semillas_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_image-cache-generator:image-cache-generator
  $menu_links['management_image-cache-generator:image-cache-generator'] = array(
    'menu_name' => 'management',
    'link_path' => 'image-cache-generator',
    'router_path' => 'image-cache-generator',
    'link_title' => 'Image cache generator',
    'options' => array(
      'identifier' => 'management_image-cache-generator:image-cache-generator',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Image cache generator');

  return $menu_links;
}
