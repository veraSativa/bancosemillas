<?php
/**
 * Created by PhpStorm.
 * User: N
 * Date: 2/20/16
 * Time: 06:08
 */

function bs_recommender_seedsearch_form($form, &$form_state) {
  // Como se hace esto bien?
  if(empty($form)){
    $form = array();
  }
  $form['#tree'] = TRUE;
  //
    /*
  // Build the intro step
  $form['intro'] = array(
    '#title' => t('Encuentra tu cepa ideal de cannabis'),
    '#type' => 'fieldset',
  );
  $form['intro']['description'] = array(
    '#markup' => t('Texto que explica este wirzard'),
  );
    */
  //
  // Build flavors step
  $form['flavors'] = bs_recommender_seedsearch_form_checkboxes('pd_seed_flavor');
  $form['flavors']['#type'] = 'fieldset';
  $form['flavors']['#title'] = t('¿Que sabores te gustan?');
  $form['flavors']['#attributes'] = array(
      'class' => array(
          'flavors_container',
      ),
  );
  //
  // Build the activity step
  $form['activities'] = bs_recommender_seedsearch_form_checkboxes('pd_seed_activity');
  $form['activities']['#type'] = 'fieldset';
  $form['activities']['#title'] = t('¿Para que actividades o estados de animo quieres tu weed?');
  $form['activities']['#attributes'] = array(
      'class' => array(
          'effects_container',
      ),
  );
  //
  // Build the medical uses step
  $form['medical'] = bs_recommender_seedsearch_form_checkboxes('pd_seed_medical');
  $form['medical']['#type'] = 'fieldset';
  $form['medical']['#title'] = t('¿Para que usos medicinales quieres tu weed?');
  $form['medical']['#attributes'] = array(
      'class' => array(
          'medical_container',
      ),
  );
  //
  // Build the growing step
  $form['growing']['#type'] = 'fieldset';
  $form['growing']['#title'] = t('Como será el cultivo?');
  $form['growing']['#attributes'] = array(
    'class' => array(
      'bs_growing_form_container',
    ),
  );
  $form['growing']['environment']['kind'] = array(
    '#type' => 'select',
    '#title' => t('Lugar de cultivo'),
    '#options' => array(
      'indoor' => t('Indoor / closet'),
      'outdoor' => t('Exterior / patio'),
      'greenhouse' => t('Invernadero'),
    ),
    '#description' => t('¿En que tipo de lugar físico te gustaría cultivar?'),
  );
  $form['growing']['environment']['latitude'] = array(
    '#type' => 'select',
    '#title' => t('Régimen lumínico'),
    '#options' => array(
      'tropical' => t('Latitud Tropical'),
      'mild' => t('Latitud Media'),
      'polar' => t('Latitud Polar'),
    ),
    '#description' => t('Si tienes un invernadero normal, solo responde de acuerdo a tu latitud. Si manipulas las horas luz artificialmente, ajusta de acuerdo a la duración de los ciclos de luz que implementarás'),
    '#states' => array(
      'visible' => array(
        'select[name="growing[environment][kind]"]' => array('value' => 'greenhouse'),
      ),
    ),
  );
  $form['growing']['environment']['climate'] = array(
      '#type' => 'select',
      '#title' => t('Clima'),
      '#options' => array(
          'hot' => t('Tropical / Caluroso'),
          'mild' => t('Templado'),
          'cold' => t('Frio'),
      ),
      '#description' => t('¿que tipo de clima tienes en la zona de cultivo?'),
      '#states' => array(
          'visible' => array(
              'select[name="growing[environment][kind]"]' => array('value' => 'outdoor'),
          ),
      ),
  );
  $form['growing']['expertice'] = array(
      '#type' => 'radios',
      '#title' => t('¿Cuanta experiencia tienes cultivando?'),
      '#default_value' => 0,
      '#options' => array(
          0 => t('<b>Ninguna o muy poca:</b> Quiero algo fácil y seguro.'),
          1 => t('<b>Media:</b> Sé cuidar mis plantas, pero no me creo ningún Cervantes'),
          2 => t('<b>Alta:</b> Estoy buscando algo interesante; algo que pueda ganar una copa'),
      ),
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Buscar mi cepa ideal'),
  );
  return $form;
}

function bs_recommender_seedsearch_form_submit(&$form, &$form_state) {
    $flavors = array();
    foreach($form['flavors'] as $nid => $data){
        if(is_numeric($nid)){
          if($data['#value'] == 1){
            $flavors[] = $nid;
          }
        }
    }
    unset($nid);
    $activities = array();
    foreach($form['activities'] as $nid => $data){
        if(is_numeric($nid)){
            if($data['#value'] == 1){
                $activities[] = $nid;
            }
        }
    }
    unset($nid);
    $medical = array();
    foreach($form['medical'] as $nid => $data){
        if(is_numeric($nid)){
            if($data['#value'] == 1){
                $medical[] = $nid;
            }
        }
    }
    unset($nid);
    $environment = 0;
  if($form['growing']['environment']['kind']['#value'] == 'indoor'){
    $environment = 0;
  } elseif($form['growing']['environment']['kind']['#value'] == 'greenhouse'){
    if($form['growing']['environment']['latitude']['#value'] == 'tropical'){
      $environment = 1;
    } elseif($form['growing']['environment']['latitude']['#value'] == 'mild'){
      $environment = 2;
    } elseif($form['growing']['environment']['latitude']['#value'] == 'polar'){
      $environment = 3;
    }
  } elseif($form['growing']['environment']['kind']['#value'] == 'outdoor'){
    if($form['growing']['environment']['climate']['#value'] == 'hot'){
      $environment = 4;
    } elseif($form['growing']['environment']['climate']['#value'] == 'mild'){
      $environment = 5;
    } elseif($form['growing']['environment']['climate']['#value'] == 'cold'){
      $environment = 6;
    }
  }
  $expertice = $form['growing']['expertice']['#value'];


/*
    dpm($flavors);
    dpm($activities);
    dpm($medical);
    dpm($environment);
    dpm($expertice);

*/

    // Create an Entity.
    $node = entity_create('node', array('type' => 'recommender_result_seed'));
    // Specify the author.
    global $user;
    $node->uid = $user->uid;
    // Create a Entity Wrapper of that new Entity.
    $emw_node = entity_metadata_wrapper('node', $node);
    // Set a title and some text field value.
    $emw_node->title = 'Temp title';
    $emw_node->field_ref_pd_seed_flavor = $flavors;
    $emw_node->field_ref_pd_seed_activity = $activities;
    $emw_node->field_ref_pd_seed_medical = $medical;
    $emw_node->field_recommended_env[] = $environment;
    $emw_node->field_recommended_expertice[] = $expertice;
    // And save it.
    $emw_node->save();
    // Title
    $title = md5('recommender_result_seed_'.$emw_node->getIdentifier());
    $emw_node->title =  $title;
    $emw_node->save();

    $_SESSION['bs_recommender_owner_nid'] = $emw_node->getIdentifier();
    drupal_goto("node/".$emw_node->getIdentifier());
}


/*
 * This function create checkboxs for each (published) node from a $node_type
 */

function bs_recommender_seedsearch_form_checkboxes($node_type, $style = 'thumbnail') {

  // DB quering
  $select = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 1)
      ->condition('type', $node_type)
      ->orderBy('title')
      ->execute();
  $nids = $select->fetchCol();
  $flavors = node_load_multiple($nids);
  unset($select);
  unset($nids);
  //Build the rows.
  foreach ($flavors as $node) {
    // Render with a custom image style, in this case named 'post_avatar'
    $icon = array(
        'style_name' => $style,
        'path' => $node->field_icon['und'][0]['uri'],
        'width' => '50',
        'height' => '50',
        'attributes' => array(
            'width' => '50',
            'height' => '50',
        ),
    );
    $return[$node->nid] = array(
        '#type' => 'checkbox',
        //'#default_value' => 1,
        '#title' => '<p>'.$node->title.'</p>'.theme_image_style($icon),
        '#wrapper_attributes' => array(
            'class' => array(
                'col-xs-6',
                'col-md-3',
                'col-lg-2',
                'checkbox-'.$node_type,
            ),
        ),
    );
  }
  return $return;
}