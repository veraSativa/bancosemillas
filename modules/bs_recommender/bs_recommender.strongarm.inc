<?php
/**
 * @file
 * bs_recommender.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function bs_recommender_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'auto_entitylabel_pattern_node_recommender_result_seed';
  $strongarm->value = 'rrs-[node:nid]';
  $export['auto_entitylabel_pattern_node_recommender_result_seed'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_recommender_result_seed';
  $strongarm->value = 0;
  $export['node_submitted_recommender_result_seed'] = $strongarm;

  return $export;
}
