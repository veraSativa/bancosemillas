# Pendientes esta iterración al May 1, 2016 #
- featurizar el content_type y lo que se necesite para andar
- renderizar los resultados
- ingresar datos coerentes en usos medicinales y estados de animo / actividades
- descomentar código en seedsearch_form.js

# Pendientes para siguiente iteración #
- Hacer que los resultados del test sean un bundle de una entidad test_results
