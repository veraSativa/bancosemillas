/**
 * Created by N on 2/25/16.
 */
(function ($){
    Drupal.behaviors.seedSearchFrom = {
        attach: function (context) {
            // Some format for the dom
            $("#bs-recommender-seedsearch-form div fieldset legend").unwrap();
            $('div .panel-body').replaceWith(function(){
                return $("<section />").append($(this).contents());
            });
            $("#bs-recommender-seedsearch-form #edit-submit").hide();

            // Get the css file
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: '/' + Drupal.settings.bs_recommender.path + '/css/seedsearch_form.css'
            }).appendTo("head");


            // Multi-step init
            var multiStep;
            multiStep = $("#bs-recommender-seedsearch-form div").steps({
                headerTag: "legend",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true,
                onFinished: function (event, currentIndex) {
                    //alert("desde steps");
                    $("#bs-recommender-seedsearch-form #edit-submit").click();
                },
            });

            // Fix the drupal form status break
            $(".form-item-growing-environment-latitude").hide();
            $(".form-item-growing-environment-climate").hide();
            $("#edit-growing-environment-kind").on('change', function(){
                if($(this).val() == "outdoor"){
                    $(".form-item-growing-environment-climate").show();
                    $(".form-item-growing-environment-latitude").hide();
                }
                if($(this).val() == "greenhouse"){
                    $(".form-item-growing-environment-latitude").show();
                    $(".form-item-growing-environment-climate").hide();
                }
                if($(this).val() == "indoor"){
                    $(".form-item-growing-environment-latitude").hide();
                    $(".form-item-growing-environment-climate").hide();
                }
            });
        }
    };
})(jQuery);