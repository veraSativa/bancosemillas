<?php
/**
 * @file
 * picec_home_page.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function picec_home_page_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'semillas';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
