<?php
/**
 * @file
 * bs_images.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function bs_images_image_default_styles() {
  $styles = array();

  // Exported image style: bs_full.
  $styles['bs_full'] = array(
    'label' => 'BS Full (watermark)',
    'effects' => array(
      3 => array(
        'name' => 'canvasactions_file2canvas',
        'data' => array(
          'xpos' => 'center',
          'ypos' => 'bottom',
          'alpha' => 100,
          'scale' => 100,
          'path' => 'private://default_images/water-mark.png',
          '#imageinfo_cache_effect_callback' => 'canvasactions_file2canvas_effect',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: bs_medium.
  $styles['bs_medium'] = array(
    'label' => 'BS Medium',
    'effects' => array(
      2 => array(
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 420,
          'height' => 420,
          'strength' => 'high',
          'fallback' => 'smartcrop',
          '#imageinfo_cache_effect_callback' => 'imagefield_focus_scale_and_crop_effect',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: bs_micro-thumbnail.
  $styles['bs_micro-thumbnail'] = array(
    'label' => 'BS Micro-thumbnail',
    'effects' => array(
      4 => array(
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 60,
          'height' => 90,
          'strength' => 'high',
          'fallback' => 'smartcrop',
          '#imageinfo_cache_effect_callback' => 'imagefield_focus_scale_and_crop_effect',
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: bs_thumbnail.
  $styles['bs_thumbnail'] = array(
    'label' => 'BS Thumbnail',
    'effects' => array(
      1 => array(
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 180,
          'height' => 180,
          'strength' => 'high',
          'fallback' => 'smartcrop',
          '#imageinfo_cache_effect_callback' => 'imagefield_focus_scale_and_crop_effect',
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
