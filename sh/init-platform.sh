#!/bin/bash
#
# SQL Removal
platform drush sql-drop
#
# Files removal
platform ssh 'rm -R ~/private/styles'
platform ssh 'rm -R ~/private/img'
platform ssh 'rm -R ~/private/feeds'
drush @banco-semillas.master site-install minimal --sites-subdir=default --account-name=nahuel --account-pass=password --account-mail=nahcar@gmail.com --site-name=Banco\ Semillas --site-mail=web@bancosemillas.cl install_configure_form.site_default_country=CL install_configure_form.date_default_timezone=America\/Santiago install_configure_form.update_status_module=array\(true, false\) -y
#
# Base modules
drush @banco-semillas.master en adminimal -y
drush @banco-semillas.master en bootstrap -y # theme
drush @banco-semillas.master en banco_semillas_bss -y # theme
drush @banco-semillas.master en base_store -y
drush @banco-semillas.master en base_semillas -y
drush @banco-semillas.master en shipping -y
drush @banco-semillas.master en payment -y
drush @banco-semillas.master en picec_home_page -y
drush @banco-semillas.master en picec_checkout -y
drush @banco-semillas.master en bs_seo -y
drush @banco-semillas.master en update -y
drush @banco-semillas.master vset theme_default banco_semillas_bss
# drush @banco-semillas.master en search_semillas -y
#
# Temp fix: core issue, google: drupal private_default_image_fix
#
drush @banco-semillas.master en private_default_image_fix -y
#
#drush @banco-semillas.master block-disable --module=system --delta=navigation --theme=adminimal
# Pushed SQL
echo "Running sql ducktaped: blocks disable"
cat sql/disable_blocks.sql | platform -y drush sql-cli
echo "Running sql ducktaped: default image"
cat sql/semilla_image_default.sql | platform -y drush sql-cli
echo "Running sql ducktaped: khipu accounts"
cat sql/khipu_accounts.sql | platform -y drush sql-cli
#
# Base files
echo "Uploading default images"
rsync -v -r ../private/default_images/. qyndb2ynrrxby-master@ssh.us.platform.sh:private/default_images/
echo "Uploading import images"
rsync -v -r ../private/import/. qyndb2ynrrxby-master@ssh.us.platform.sh:private/import/
# Local Macros
#echo "Importing trough firefox imacros"
# /Applications/Firefox.app/Contents/MacOS/firefox -P macrero imacros://run/?m=bs-platform-import.iim
echo "Run bs-local-import.imm at you dev FF"