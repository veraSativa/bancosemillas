#!/bin/bash
base_dir="$(pwd)"
cd ../www
drush sql-drop
#
#echo "deleting files"
# cachar donde quedan estos archivos ahora y cuidado con no cagarlos con featurizer
#rm -R $base_dir/.../private/styles
#rm -R $base_dir/.../private/img
#rm -R $base_dir/.../private/feeds
#
drush site-install minimal --sites-subdir=default --account-name=nahuel --account-pass=password --account-mail=nahcar@gmail.com --site-name=LOCAL\ Banco\ Semillas --site-mail=web@bancosemillas.cl install_configure_form.site_default_country=CL install_configure_form.date_default_timezone=America\/Santiago install_configure_form.update_status_module=array\(true, false\) -y
drush en adminimal -y # admin theme
drush en bootstrap -y # base theme
drush en banco_semillas_bss -y # custom theme
# Features
drush en base_store -y
drush en base_semillas -y
drush en shipping -y
drush en payment -y
drush en picec_home_page -y
drush en picec_checkout -y
drush en bs_seo -y
drush en bs_theme_bootstrap -y
drush en bs_wysiwyg -y
# Update module (just to keep an eye on updates here and there)
drush en update -y
# Set custom theme as default
drush vset theme_default banco_semillas_bss
#
# For this you need drush_extras installed, install command:
# $ drush dl drush_extras
#
drush block-disable --module=system --delta=navigation --theme=adminimal
drush block-disable --module=system --delta=management --theme=adminimal
drush block-disable --module=system --delta=navigation --theme=banco_semillas_bss
drush block-disable --module=system --delta=management --theme=banco_semillas_bss
drush block-disable --module=user --delta=login --theme=banco_semillas_bss
#
#
drush en private_default_image_fix -y
drush vset file_private_path "../../../private"
#
#
echo "Running sql ducktaped: default image"
cat $base_dir/sql/semilla_image_default.sql | drush sql-cli
echo "Running sql ducktaped: khipu accounts"
cat $base_dir/sql/khipu_accounts.sql | drush sql-cli
#/Applications/Firefox.app/Contents/MacOS/firefox -P Dev imacros://run/?m=local-import.iim
echo "Run bs-local-import.imm at you dev FF"